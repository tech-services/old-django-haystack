# encoding: utf-8

import warnings

from django.conf import settings

from ..utils import unittest

warnings.simplefilter('ignore', Warning)

def setup():
    try:
        from legacy_elasticsearch import Elasticsearch, ElasticsearchException
    except ImportError:
        raise unittest.SkipTest("legacy_elasticsearch-py not installed.")

    es = Elasticsearch(settings.HAYSTACK_CONNECTIONS['legacy_elasticsearch']['URL'])
    try:
        es.info()
    except ElasticsearchException as e:
        raise unittest.SkipTest("legacy_elasticsearch not running on %r" % settings.HAYSTACK_CONNECTIONS['legacy_elasticsearch']['URL'], e)

